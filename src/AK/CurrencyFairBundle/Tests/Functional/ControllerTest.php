<?php

namespace AK\CurrencyFairBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class ControllerTest extends WebTestCase
{
    /** @var Client */
    private $client;

    private $messageData = array(
        'userId'             => 1,
        'currencyFrom'       => 'ABC',
        'currencyTo'         => 'DEF',
        'amountSell'         => 1,
        'amountBuy'          => 2,
        'rate'               => 3,
        'timePlaced'         => '27-MAY-2015 01:01:01',
        'originatingCountry' => 'IE'
    );

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testInputDoesntAcceptGET()
    {
        $this->client->request('GET', '/input/');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    public function testInputAcceptsPOST()
    {
        $this->client->request('POST', '/input/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testInputReturnsJSONResponse()
    {
        $this->client->request('POST', '/input/');

        $this->assertSame('application/json', $this->client->getResponse()->headers->get('Content-Type'));
    }

    public function testInputReturnsTrue()
    {
        $json = json_encode($this->messageData);
        $this->client->request('POST', '/input/', array(), array(), array(), $json);
        $responseJson = $this->client->getResponse()->getContent();
        $responseData = json_decode($responseJson, true);

        $this->assertTrue($responseData['status']);
    }

    /**
     * @dataProvider providerMissingKey
     * @param string $missingKey
     * @param array  $messageData
     */
    public function testInputReturnsFalseOnMissingKey($missingKey, $messageData)
    {
        $json = json_encode($messageData);
        $this->client->request('POST', '/input/', array(), array(), array(), $json);
        $responseJson = $this->client->getResponse()->getContent();
        $responseData = json_decode($responseJson, true);

        $this->assertFalse($responseData['status']);
        $this->assertSame(
            sprintf('The following message keys are missing: %s', $missingKey),
            $responseData['error']
        );
    }

    public function testOutputReturnsJSONResponse()
    {
        $this->client->request('POST', '/output/');

        $this->assertSame('application/json', $this->client->getResponse()->headers->get('Content-Type'));
    }

    public function testOutputReturnsRequiedKeys()
    {
        $this->client->request('POST', '/output/');
        $responseJson = $this->client->getResponse()->getContent();
        $responseData = json_decode($responseJson, true);

        $this->assertArrayHasKey('transactions', $responseData);
        $this->assertArrayHasKey('queue', $responseData);
        $this->assertArrayHasKey('count', $responseData['transactions']);
        $this->assertArrayHasKey('trade', $responseData['transactions']);
        $this->assertArrayHasKey('users', $responseData['transactions']);
        $this->assertArrayHasKey('countries', $responseData['transactions']);
        $this->assertArrayHasKey('count', $responseData['queue']);
    }

    public function providerMissingKey()
    {
        $dataMissingUserId
            = $dataMissingCurrencyFrom
            = $dataMissingCurrencyTo
            = $dataMissingAmountSell
            = $dataMissingAmountBuy
            = $dataMissingRate
            = $dataMissingTimePlaced
            = $dataMissingOriginatingCountry
            = $this->messageData;
        unset($dataMissingUserId['userId']);
        unset($dataMissingCurrencyFrom['currencyFrom']);
        unset($dataMissingCurrencyTo['currencyTo']);
        unset($dataMissingAmountSell['amountSell']);
        unset($dataMissingAmountBuy['amountBuy']);
        unset($dataMissingRate['rate']);
        unset($dataMissingTimePlaced['timePlaced']);
        unset($dataMissingOriginatingCountry['originatingCountry']);

        return array(
            array('userId', $dataMissingUserId),
            array('currencyFrom', $dataMissingCurrencyFrom),
            array('currencyTo', $dataMissingCurrencyTo),
            array('amountSell', $dataMissingAmountSell),
            array('amountBuy', $dataMissingAmountBuy),
            array('rate', $dataMissingRate),
            array('timePlaced', $dataMissingTimePlaced),
            array('originatingCountry', $dataMissingOriginatingCountry)
        );
    }
}
