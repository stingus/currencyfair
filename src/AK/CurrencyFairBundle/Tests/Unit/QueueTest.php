<?php

namespace AK\CurrencyFairBundle\Tests\Unit;

use AK\CurrencyFairBundle\Message\Queue;
use AK\CurrencyFairBundle\Message\QueueProvider;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class QueueTest extends TestCase
{
    /** @var Serializer */
    private $serializer;

    /** @var QueueProvider */
    private $queueProvider;

    /** @var Queue */
    private $queue;

    private $requiredKeys = array(
        'userId',
        'currencyFrom',
        'currencyTo',
        'amountSell',
        'amountBuy',
        'rate',
        'timePlaced',
        'originatingCountry'
    );

    private $messageData = array(
        'userId'             => 1,
        'currencyFrom'       => 'ABC',
        'currencyTo'         => 'DEF',
        'amountSell'         => 1,
        'amountBuy'          => 2,
        'rate'               => 3,
        'timePlaced'         => '27-MAY-2015 01:01:01',
        'originatingCountry' => 'IE'
    );

    public function setUp()
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $this->serializer = new Serializer($normalizers, $encoders);

        $this->queueProvider = $this->getMockBuilder('AK\CurrencyFairBundle\Message\QueueProvider')
            ->setMethods(array())
            ->getMock();

        $this->queue = new Queue();
        $this->queue
            ->setQueueProvider($this->queueProvider)
            ->setRequiredKeys($this->requiredKeys)
            ->setSerializer($this->serializer);
    }

    public function testQueueValidMessage()
    {
        $result = $this->queue
            ->queueMessage(json_encode($this->messageData));

        $this->assertTrue($result['status']);
    }

    /**
     * @dataProvider providerMissingKey
     * @param string $missingKey
     * @param array  $messageData
     */
    public function testQueueInvalidMessageWithMissingKeys($missingKey, $messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are missing: %s', $missingKey),
            $result['error']
        );
    }

    /**
     * @dataProvider providerInvalidUserId
     * @param array $messageData
     */
    public function testQueueInvalidUserId($messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are invalid: userId', $messageData),
            $result['error']
        );
    }

    /**
     * @dataProvider providerInvalidCurrencyFrom
     * @param array $messageData
     */
    public function testQueueInvalidCurrencyFrom($messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are invalid: currencyFrom', $messageData),
            $result['error']
        );
    }

    /**
     * @dataProvider providerInvalidCurrencyTo
     * @param array $messageData
     */
    public function testQueueInvalidCurrencyTo($messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are invalid: currencyTo', $messageData),
            $result['error']
        );
    }

    /**
     * @dataProvider providerInvalidAmountSell
     * @param array $messageData
     */
    public function testQueueInvalidAmountSell($messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are invalid: amountSell', $messageData),
            $result['error']
        );
    }

    /**
     * @dataProvider providerInvalidAmountBuy
     * @param array $messageData
     */
    public function testQueueInvalidAmountBuy($messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are invalid: amountBuy', $messageData),
            $result['error']
        );
    }

    /**
     * @dataProvider providerInvalidRate
     * @param array $messageData
     */
    public function testQueueInvalidAmountRate($messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are invalid: rate', $messageData),
            $result['error']
        );
    }

    /**
     * @dataProvider providerInvalidTimePlaced
     * @param array $messageData
     */
    public function testQueueInvalidTimePlaced($messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are invalid: timePlaced', $messageData),
            $result['error']
        );
    }

    /**
     * @dataProvider providerInvalidOriginatingCountry
     * @param array $messageData
     */
    public function testQueueInvalidOriginatingCountry($messageData)
    {
        $result = $this->queue
            ->queueMessage(json_encode($messageData));

        $this->assertFalse($result['status']);
        $this->assertSame(
            sprintf('The following message keys are invalid: originatingCountry', $messageData),
            $result['error']
        );
    }

    public function providerMissingKey()
    {
        $dataMissingUserId
            = $dataMissingCurrencyFrom
            = $dataMissingCurrencyTo
            = $dataMissingAmountSell
            = $dataMissingAmountBuy
            = $dataMissingRate
            = $dataMissingTimePlaced
            = $dataMissingOriginatingCountry
            = $this->messageData;
        unset($dataMissingUserId['userId']);
        unset($dataMissingCurrencyFrom['currencyFrom']);
        unset($dataMissingCurrencyTo['currencyTo']);
        unset($dataMissingAmountSell['amountSell']);
        unset($dataMissingAmountBuy['amountBuy']);
        unset($dataMissingRate['rate']);
        unset($dataMissingTimePlaced['timePlaced']);
        unset($dataMissingOriginatingCountry['originatingCountry']);

        return array(
            array('userId', $dataMissingUserId),
            array('currencyFrom', $dataMissingCurrencyFrom),
            array('currencyTo', $dataMissingCurrencyTo),
            array('amountSell', $dataMissingAmountSell),
            array('amountBuy', $dataMissingAmountBuy),
            array('rate', $dataMissingRate),
            array('timePlaced', $dataMissingTimePlaced),
            array('originatingCountry', $dataMissingOriginatingCountry)
        );
    }

    public function providerInvalidUserId()
    {
        $invalidUserId = array(
            'a',
            true,
            false,
            null,
            -1,
            -0.01,
            0,
            18446744073709551616
        );

        return $this->getMessageProviders('userId', $invalidUserId);
    }

    public function providerInvalidCurrencyFrom()
    {
        return $this->getMessageProviders('currencyFrom', $this->getInvalidCurrency());
    }

    public function providerInvalidCurrencyTo()
    {
        return $this->getMessageProviders('currencyTo', $this->getInvalidCurrency());
    }

    public function providerInvalidAmountSell()
    {
        return $this->getMessageProviders('amountSell', $this->getInvalidAmount());
    }

    public function providerInvalidAmountBuy()
    {
        return $this->getMessageProviders('amountBuy', $this->getInvalidAmount());
    }

    public function providerInvalidRate()
    {
        return $this->getMessageProviders('rate', $this->getInvalidAmount());
    }

    public function providerInvalidTimePlaced()
    {
        $invalidTimePlaced = array(
            true,
            false,
            null,
            '32-MAY-2015 00:00:00',
            '01-ABC-2015 00:00:00',
            '01-MAY-a015 00:00:00',
            '01-MAY-2015 a0:00:00',
            '01-MAY-2015 00:a0:00',
            '01-MAY-2015 00:00:a0',
            '01.MAY.2015 00:00:00',
            '01/MAY/2015 00:00:00',
            '01/MAY/201500:00:00',
            '01-MAY-2015',
            '01-MAY-2015 25:00:00',
            '01-MAY-2015 00:60:00',
            '01-MAY-2015 00:00:60'
        );

        return $this->getMessageProviders('timePlaced', $invalidTimePlaced);
    }

    public function providerInvalidOriginatingCountry()
    {
        $invalidTimePlaced = array(
            true,
            false,
            null,
            'a',
            'A',
            'ABC',
            '1A',
            'A1',
            1,
            11
        );

        return $this->getMessageProviders('originatingCountry', $invalidTimePlaced);
    }

    private function getMessageProviders($key, array $values)
    {
        $providers = array();
        foreach ($values as $value) {
            $messageData = $this->messageData;
            $messageData[$key] = $value;
            $providers[] = array($messageData);
        }

        return $providers;
    }

    private function getInvalidCurrency()
    {
        return array(
            'abc',
            'ABCD',
            'AB1',
            '1AB',
            'AB',
            true,
            false,
            null,
            1,
            0,
            .1
        );
    }

    private function getInvalidAmount()
    {
        return array(
            'a',
            true,
            false,
            null,
            -1,
            -0.01,
            0,
            18446744073709551615.01
        );
    }
}
