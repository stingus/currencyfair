<?php
/**
 * Contains the QueueProducerCommand
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class QueueProducerCommand
 * Message producer, can add messages via HTTP POST or directly into storage
 *
 * @package AK\CurrencyFairBundle\Command
 */
class QueueProducerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:produce')
            ->setDescription('Message queue producer')
            ->addArgument(
                'messageNo',
                InputArgument::REQUIRED,
                'How many messages to queue?'
            )
            ->addOption(
                'http',
                null,
                InputOption::VALUE_OPTIONAL,
                'Add messages via http request',
                null
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $messageNo = $input->getArgument('messageNo');
        $iteration = 0;

        if ($input->getOption('http')) {
            $url = $this->getContainer()->get('router')->generate('ak_currency_fair_input', array(), true);
            $mh = curl_multi_init();
            $i = 0;

            do {
                $ch[++$i] = curl_init();
                curl_setopt($ch[$i], CURLOPT_URL, $url);
                curl_setopt($ch[$i], CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, true);

                $json = json_encode($this->getRandomData());
                curl_setopt($ch[$i], CURLOPT_POSTFIELDS, $json);
                curl_setopt(
                    $ch[$i],
                    CURLOPT_HTTPHEADER,
                    array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($json)
                    )
                );
                curl_multi_add_handle($mh, $ch[$i]);
                $iteration++;
            } while ($iteration < $messageNo);

            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM || $active);

            foreach ($ch as $curl) {
                curl_multi_remove_handle($mh, $curl);
            }
            curl_multi_close($mh);
        } else {
            $queue = $this->getContainer()->get('fair.queue');

            do {
                $data = $this->getRandomData();
                $queue->queueMessage(json_encode($data));
                $iteration++;
            } while ($iteration < $messageNo);
        }
    }

    /**
     * Generate random message data
     *
     * @return array
     */
    private function getRandomData()
    {
        $countries = array('IE', 'FR', 'GB', 'DE', 'US');
        $currencies = array('EUR', 'GBP', 'USD', 'CHF');
        $from = $currencies[array_rand($currencies, 1)];
        $currencies = array_filter($currencies, function ($value) use ($from) {
            return $from != $value;
        });
        $to = $currencies[array_rand($currencies, 1)];
        $sell = $this->randFloat(1, 100000);
        $buy = $this->randFloat(1, 100000);
        $ratio = round($buy / $sell, 2);
        return array(
            'userId'             => mt_rand(1, 10),
            'currencyFrom'       => $from,
            'currencyTo'         => $to,
            'amountSell'         => $this->randFloat(1, 100000),
            'amountBuy'          => $this->randFloat(1, 100000),
            'rate'               => $ratio,
            'timePlaced'         => date('d-M-Y H:i:s'),
            'originatingCountry' => $countries[array_rand($countries)]
        );
    }

    /**
     * Generate a random float with 2 decimals
     *
     * @param float $min Minimum value
     * @param float $max Maximum value
     * @return float
     */
    private function randFloat($min, $max)
    {
        return round(($min + mt_rand() / mt_getrandmax() * ($max - $min)), 2);
    }
}
