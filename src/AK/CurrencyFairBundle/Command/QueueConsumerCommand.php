<?php
/**
 * Contains the QueueConsumerCommand
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class QueueConsumerCommand
 * Message queue consumer
 *
 * @package AK\CurrencyFairBundle\Command
 */
class QueueConsumerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('queue:consume')->setDescription('Message queue consumer');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('fair.consumer')->consume();
    }
}
