<?php
/**
 * Contains StorageProviderRedis
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\StorageProvider;

/**
 * Class StorageProviderRedis.
 * Storage implementation for the Redis server
 */
class StorageProviderRedis implements StorageProviderInterface
{
    /** @var \Redis Redis client */
    protected $provider;

    /**
     * Get the storage provider client
     *
     * @return \Redis
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set the storage provider client
     *
     * @param \Redis $provider Redis client
     * @return $this
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
        $this->provider->setOption(\Redis::OPT_READ_TIMEOUT, -1);

        return $this;
    }

    /**
     * Push an item to the end of a collection
     *
     * @param string $collectionName Collection name where the item will be added
     * @param string $item           Item to be added
     * @return bool
     * @throws \Exception
     */
    public function pushItemToCollection($collectionName, $item)
    {
        $result = $this->executeCommand('rpush', array($collectionName, $item));
        if (!$result) {
            throw new \Exception('Item could not be added to collection');
        } else {
            return true;
        }
    }

    /**
     * Get and remove the oldest item from a collection.
     * Wait for StorageProviderInterface::WAIT_TIMEOUT if the collection is empty
     *
     * @param string $collectionName The collection name
     * @return string | null
     */
    public function getOldestItemFromCollection($collectionName)
    {
        $result = $this->executeCommand('blpop', array($collectionName, StorageProviderInterface::WAIT_TIMEOUT));
        if (!empty($result) && array_key_exists(1, $result)) {
            $message = $result[1];
        } else {
            $message = null;
        }

        return $message;
    }

    /**
     * Get the size of a collection
     *
     * @param string $collectionName The collection name
     * @param string $type           The type of the collection
     * @return int
     * @throws \Exception
     */
    public function getCollectionSize($collectionName, $type = 'list')
    {
        if ($type == 'list') {
            $result = $this->executeCommand('llen', array($collectionName));
        } elseif ($type == 'hash') {
            $result = count($this->getHashKeys($collectionName));
        } else {
            throw new \Exception($type . ' is an unknown collection type');
        }

        return $result;
    }

    /**
     * Get collection values
     *
     * @param string $collectionName The collection name
     * @return array
     */
    public function getCollectionValues($collectionName)
    {
        $results = array();
        $keys = $this->getHashKeys($collectionName);
        if (is_array($keys)) {
            foreach ($keys as $key) {
                $values = $this->executeCommand('hgetall', array($key));
                if (is_array($values)) {
                    $results[$key] = $values;
                }
            }
        }

        return $results;
    }

    /**
     * Save data in a collection.
     * Data is saved in a Redis hash.
     * If generateId is true, get an auto-increment Id for this transaction
     *
     * @param string $collectionName The collection name
     * @param array  $data           Data to be stored
     * @param bool   $generateId     Whether or not to generate an Id for this transaction
     * @return int | bool            Int if the Id was requested and generated
     * @throws \Exception
     */
    public function saveData($collectionName, array $data, $generateId = false)
    {
        if ($generateId === true) {
            $id = $this->executeCommand('incr', array($collectionName . '_id'));
            if (!empty($id)) {
                $collectionName .= ':' . $id;
            } else {
                throw new \Exception('Transaction Id requested but could not be generated');
            }
        }
        $result = $this->executeCommand('hmset', array($collectionName, $data));
        if (!empty($result) && !empty($id)) {
            $result = (int) $id;
        }

        return $result;
    }

    /**
     * Increment a field value
     *
     * @param string $collectionName The collection name
     * @param string $field          Field name to increment
     * @param mixed  $increment      Increment value
     * @return int | bool            Int if the field was successfully incremented
     * @throws \Exception
     */
    public function incrementField($collectionName, $field, $increment)
    {
        if (is_int($increment)) {
            $command = 'hincrby';
        } elseif (is_float($increment)) {
            $command = 'hincrbyfloat';
        } else {
            throw new \Exception('Incremennt must be int or float');
        }
        $result = $this->executeCommand($command, array($collectionName, $field, $increment));
        if ($result) {
            if (is_int($increment)) {
                $result = (int) $result;
            } elseif (is_float($increment)) {
                $result = (float) $result;
            }
        }

        return $result;
    }

    /**
     * Execute a Redis command (wrapper)
     *
     * @param string $command  Redis command to be executed
     * @param array $arguments Array of arguments
     * @return mixed
     */
    private function executeCommand($command, array $arguments)
    {
        return call_user_func_array(array($this->getProvider(), $command), $arguments);
    }

    /**
     * Get hash keys.
     * Retrive the keys matching the collection name prefix
     *
     * @param string $collectionNamePrefix Collection name prefix (will be suffixed with *)
     * @return array
     */
    private function getHashKeys($collectionNamePrefix)
    {
        $results = array();
        $cursor = null;
        $this->getProvider()->setOption(\Redis::OPT_SCAN, \Redis::SCAN_RETRY);
        while ($items = $this->executeCommand('scan', array(&$cursor, $collectionNamePrefix . '*'))) {
            if (is_array($items) && !empty($items)) {
                $results = array_merge($results, $items);
            }
        }

        return $results;
    }
}
