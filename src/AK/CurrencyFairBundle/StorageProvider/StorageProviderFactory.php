<?php
/**
 * Contains the StorageProviderFactory
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\StorageProvider;

/**
 * Class StorageProviderFactory.
 * Creates storage providers for message queue, processed data storage
 */
class StorageProviderFactory
{
    /**
     * Storage providers array.
     * The array key is the name of the service, as defined in parameters
     *
     * @var array
     */
    private $storageProviders;

    /**
     * Set the available storage providers
     *
     * @param array $storageProviders
     */
    public function setStorageProviders(array $storageProviders)
    {
        $this->storageProviders = $storageProviders;
    }

    /**
     * Create a storage provider instance
     *
     * @param string $providerType Storage provider type (Redis currently supported)
     * @return StorageProviderInterface
     * @throws \InvalidArgumentException
     */
    public function createStorageProvider($providerType)
    {
        $providerService = $this->getProviderService($providerType);
        switch ($providerType) {
            case 'redis':
                $storageProvider = new StorageProviderRedis();
                $storageProvider->setProvider($providerService);
                break;
            default:
                throw new \InvalidArgumentException($providerType . ' is not a valid storage type provider');
        }

        return $storageProvider;
    }

    /**
     * Get the storage provider service, based on its name.
     * Check whether the service is available in the storage providers array
     *
     * @param string $providerType Storage provider type
     * @return mixed
     * @throws \InvalidArgumentException
     */
    private function getProviderService($providerType)
    {
        if (array_key_exists($providerType, $this->storageProviders)) {
            return $this->storageProviders[$providerType];
        } else {
            throw new \InvalidArgumentException($providerType . ' cannot be found in the available providers');
        }
    }
}
