<?php
/**
 * Contains the StorageProviderInterface
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\StorageProvider;

/**
 * Interface StorageProviderInterface.
 * Interface providing methods to access the storage
 */
interface StorageProviderInterface
{
    /**
     * Duration (sec) to wait for an item to be retrieved from collection
     * 0 = wait indefinitely
     */
    const WAIT_TIMEOUT = 0;

    /**
     * Get the provider client
     */
    public function getProvider();

    /**
     * Set the provider client
     *
     * @param mixed $provider
     */
    public function setProvider($provider);

    /**
     * Push an item to the end of a collection
     *
     * @param string $collectionName Collection name where the item will be added
     * @param string $item           Item to be added
     * @return bool
     */
    public function pushItemToCollection($collectionName, $item);

    /**
     * Get and remove the oldest item from a collection.
     *
     * @param string $collectionName The collection name
     * @return string | null
     */
    public function getOldestItemFromCollection($collectionName);

    /**
     * Get the size of a collection
     *
     * @param string $collectionName The collection name
     * @param string $type           The type of the collection
     * @return int
     */
    public function getCollectionSize($collectionName, $type = 'list');

    /**
     * Get collection values
     *
     * @param string $collectionName The collection name
     * @return array
     */
    public function getCollectionValues($collectionName);

    /**
     * Save data in a collection.
     *
     * @param string $collectionName The collection name
     * @param array  $data           Data to be stored
     * @param bool   $generateId     Whether or not to generate an Id for this transaction
     * @return int | bool            Int if the Id was requested and generated
     */
    public function saveData($collectionName, array $data, $generateId = false);

    /**
     * Increment a field value
     *
     * @param string $collectionName The collection name
     * @param string $field          Field name to increment
     * @param mixed  $increment      Increment value
     * @return int | bool            Int if the field was successfully incremented
     */
    public function incrementField($collectionName, $field, $increment);
}
