<?php
/**
 * Contains the QueueProvider
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Message;

use AK\CurrencyFairBundle\StorageProvider\StorageProviderInterface;

/**
 * Class QueueProvider.
 * Contains methods to manage the message queue (add to queue, get from queue, get queue size)
 */
class QueueProvider
{
    /** The collection where the queue will be stored */
    const COLLECTION_QUEUE = 'fq';

    /**
     * The storage provider
     *
     * @var StorageProviderInterface
     */
    private $storageProvider;

    /**
     * The queue maximum size.
     * Messages will not be queued if this limit is reached
     *
     * @var int
     */
    private $queueLimit;

    /**
     * Get the storage provider
     *
     * @return StorageProviderInterface
     */
    public function getStorageProvider()
    {
        return $this->storageProvider;
    }

    /**
     * Set the storage provider
     *
     * @param StorageProviderInterface $storageProvider
     * @return $this
     */
    public function setStorageProvider(StorageProviderInterface $storageProvider)
    {
        $this->storageProvider = $storageProvider;

        return $this;
    }

    /**
     * Get the queue maximum size
     *
     * @return int Maximum number of messages queued
     */
    public function getQueueLimit()
    {
        return $this->queueLimit;
    }

    /**
     * Set the queue maximum size
     *
     * @param int $queueLimit Queue size limit
     * @return $this
     */
    public function setQueueLimit($queueLimit)
    {
        $this->queueLimit = $queueLimit;

        return $this;
    }

    /**
     * Add a message to queue
     *
     * @param string $message The message to be queued
     * @return bool
     * @throws \Exception
     */
    public function queue($message)
    {
        if ($this->getQueueSize() >= $this->getQueueLimit()) {
            throw new \Exception('Queue reached the maximum size limit, please try later');
        }
        try {
            $result = $this->getStorageProvider()->pushItemToCollection(self::COLLECTION_QUEUE, $message);
        } catch (\Exception $e) {
            throw new \Exception('Message could not be added to queue');
        }

        return $result;
    }

    /**
     * Get and remove the first message to be consumed from queue
     *
     * @return string | null
     */
    public function getFirstMessage()
    {
        return $this->getStorageProvider()->getOldestItemFromCollection(self::COLLECTION_QUEUE);
    }

    /**
     * Get the current queue size
     *
     * @return int
     */
    public function getQueueSize()
    {
        return $this->getStorageProvider()->getCollectionSize(self::COLLECTION_QUEUE);
    }
}
