<?php
/**
 * Contains the Extractor
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Message;

/**
 * Class Extractor.
 * Gets processed data from storage
 */
class Extractor
{
    /** @var ProcessorProvider */
    private $processorProvider;

    /** @var QueueProvider */
    private $queueProvider;

    /**
     * Get the processor provider
     *
     * @return ProcessorProvider
     */
    public function getProcessorProvider()
    {
        return $this->processorProvider;
    }

    /**
     * Get the processor provider
     *
     * @param ProcessorProvider $processorProvider
     * @return $this
     */
    public function setProcessorProvider($processorProvider)
    {
        $this->processorProvider = $processorProvider;

        return $this;
    }

    /**
     * Get the queue provider
     *
     * @return QueueProvider
     */
    public function getQueueProvider()
    {
        return $this->queueProvider;
    }

    /**
     * Set the queue provider
     *
     * @param QueueProvider $queueProvider
     * @return $this
     */
    public function setQueueProvider($queueProvider)
    {
        $this->queueProvider = $queueProvider;

        return $this;
    }

    /**
     * Get a snapshot of the stored data
     *
     * @return array
     */
    public function getSnapshot()
    {
        $processorProvider = $this->getProcessorProvider();

        $results['transactions']['count'] = $processorProvider->getTransactionCount();
        $results['transactions']['trade'] = $processorProvider->getTransactionTrade();
        $results['transactions']['users'] = $processorProvider->getTransactionUsers();
        $results['transactions']['countries'] = $processorProvider->getTransactionCountries();
        $results['queue']['count'] = $this->getQueueProvider()->getQueueSize();

        return $results;
    }
}
