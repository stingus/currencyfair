<?php
/**
 * Contains the ProcessorProvider
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Message;

use AK\CurrencyFairBundle\StorageProvider\StorageProviderInterface;

/**
 * Class ProcessorProvider.
 * Contains methods to save and extract processed data from storage
 */
class ProcessorProvider
{
    /** The collection where the transactions will be stored */
    const COLLECTION_TRANS = 'ft';

    /** The collection where the user transactions count will be stored */
    const COLLECTION_TRANS_USER = 'fu';

    /** The collection where the country transactions count will be stored */
    const COLLECTION_TRANS_COUNTRY = 'fc';

    /** The collection where the trade data will be stored*/
    const COLLECTION_TRADE = 'ftr';

    /** The field where the sell volume will be stored */
    const TRADE_FIELD_SELL = 'sell';

    /** The field where the buy volume will be stored */
    const TRADE_FIELD_BUY = 'buy';

    /** The field where the user transactions count will be stored */
    const TRANSACTION_FIELD_COUNT = 'trans';

    /**
     * The storage provider
     *
     * @var StorageProviderInterface
     */
    private $storageProvider;

    /**
     * Get the storage provider
     *
     * @return StorageProviderInterface
     */
    public function getStorageProvider()
    {
        return $this->storageProvider;
    }

    /**
     * Set the storage provider
     *
     * @param StorageProviderInterface $storageProvider
     * @return $this
     */
    public function setStorageProvider(StorageProviderInterface $storageProvider)
    {
        $this->storageProvider = $storageProvider;

        return $this;
    }

    /**
     * Save raw message as a transaction
     *
     * @param array $data Message data
     * @return bool|int
     */
    public function saveMessage(array $data)
    {
        return $this->getStorageProvider()
            ->saveData(self::COLLECTION_TRANS, array('message' => json_encode($data)), true);
    }

    /**
     * Increment the user transaction count
     *
     * @param string $userId The user Id
     * @return bool|int
     */
    public function saveUserTransaction($userId)
    {
        $result = false;
        if (!empty($userId)) {
            $collectionName = sprintf('%s:%s', self::COLLECTION_TRANS_USER, $userId);
            $result = $this->getStorageProvider()->incrementField($collectionName, self::TRANSACTION_FIELD_COUNT, 1);
        }

        return $result;
    }

    /**
     * Increment the country transaction count
     *
     * @param string $country The country code
     * @return bool|int
     */
    public function saveCountryTransaction($country)
    {
        $result = false;
        if (!empty($country)) {
            $collectionName = sprintf('%s:%s', self::COLLECTION_TRANS_COUNTRY, $country);
            $result = $this->getStorageProvider()->incrementField($collectionName, self::TRANSACTION_FIELD_COUNT, 1);
        }

        return $result;
    }

    /**
     * Save a transaction processed data
     *
     * @param array $data Message data
     * @return array | bool
     */
    public function saveTransaction(array $data)
    {
        $result = false;
        if (array_key_exists('currencyFrom', $data)) {
            $currencyFrom = strtoupper($data['currencyFrom']);
        }
        if (array_key_exists('currencyTo', $data)) {
            $currencyTo = strtoupper($data['currencyTo']);
        }
        if (array_key_exists('amountSell', $data)) {
            $sell = (float) $data['amountSell'];
        }
        if (array_key_exists('amountBuy', $data)) {
            $buy = (float) $data['amountBuy'];
        }
        if (isset($currencyFrom) && isset($currencyTo) && isset($sell) && isset($buy)) {
            $collectionName = sprintf('%s:%s:%s', self::COLLECTION_TRADE, $currencyFrom, $currencyTo);
            $result['count'] = $this->getStorageProvider()
                ->incrementField($collectionName, self::TRANSACTION_FIELD_COUNT, 1);
            $result['sell'] = $this->getStorageProvider()
                ->incrementField($collectionName, self::TRADE_FIELD_SELL, $sell);
            $result['buy'] = $this->getStorageProvider()
                ->incrementField($collectionName, self::TRADE_FIELD_BUY, $buy);
        }

        return $result;
    }

    /**
     * Get the transaction count
     *
     * @return int
     */
    public function getTransactionCount()
    {
        return $this->getStorageProvider()->getCollectionSize(self::COLLECTION_TRANS . ':', 'hash');
    }

    /**
     * Get the trade data
     *
     * @return array
     */
    public function getTransactionTrade()
    {
        $results = array();
        $trade = $this->getStorageProvider()->getCollectionValues(self::COLLECTION_TRADE);
        if (is_array($trade)) {
            foreach ($trade as $key => $value) {
                if (preg_match('/' . self::COLLECTION_TRADE . ':([A-Z]{3}):([A-Z]{3})/', $key, $currencies)) {
                    $results[$currencies[1]][$currencies[2]] = $value;
                }
            }
        }

        return $results;
    }

    /**
     * Get transaction count per user.
     * Array containing user IDs as keys as transaction count as values
     *
     * @return array
     */
    public function getTransactionUsers()
    {
        $results = array();
        $users = $this->getStorageProvider()->getCollectionValues(self::COLLECTION_TRANS_USER);
        if (is_array($users)) {
            foreach ($users as $key => $value) {
                if (preg_match('/' . self::COLLECTION_TRANS_USER . ':([0-9]+)/', $key, $user)) {
                    $results[$user[1]] = $value[self::TRANSACTION_FIELD_COUNT];
                }
            }
        }

        return $results;
    }

    /**
     * Get transaction count per country.
     * Array containing country codes as keys as transaction count as values
     *
     * @return array
     */
    public function getTransactionCountries()
    {
        $results = array();
        $countries = $this->getStorageProvider()->getCollectionValues(self::COLLECTION_TRANS_COUNTRY);
        if (is_array($countries)) {
            foreach ($countries as $key => $value) {
                if (preg_match('/' . self::COLLECTION_TRANS_COUNTRY . ':([A-Z]{2})/', $key, $country)) {
                    $results[$country[1]] = $value[self::TRANSACTION_FIELD_COUNT];
                }
            }
        }

        return $results;
    }
}
