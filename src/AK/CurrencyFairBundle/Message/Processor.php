<?php
/**
 * Contains the Processor
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Message;

use Symfony\Component\Serializer\Serializer;

/**
 * Class Processor.
 * Message processor, saves the processed information to storage
 */
class Processor
{
    /** @var ProcessorProvider */
    private $processorProvider;

    /** @var Serializer */
    private $serializer;

    /**
     * Get the processor provider
     *
     * @return ProcessorProvider
     */
    public function getProcessorProvider()
    {
        return $this->processorProvider;
    }

    /**
     * Get the processor provider
     *
     * @param ProcessorProvider $processorProvider
     * @return $this
     */
    public function setProcessorProvider($processorProvider)
    {
        $this->processorProvider = $processorProvider;

        return $this;
    }

    /**
     * Get the serializer service
     *
     * @return Serializer
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * Set the serializer service
     *
     * @param Serializer $serializer
     * @return $this
     */
    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;

        return $this;
    }

    /**
     * Process a message
     *
     * @param string $message Message in JSON format
     * @return bool
     */
    public function process($message)
    {
        $result = false;
        $processorProvider = $this->getProcessorProvider();
        $data = $this->getSerializer()->decode($message, 'json');
        if (!empty($data)) {
            $processorProvider->saveMessage($data);
            if (array_key_exists('userId', $data)) {
                $processorProvider->saveUserTransaction($data['userId']);
            }
            if (array_key_exists('originatingCountry', $data)) {
                $processorProvider->saveCountryTransaction($data['originatingCountry']);
            }
            $processorProvider->saveTransaction($data);
            $result = true;
        }

        return $result;
    }
}
