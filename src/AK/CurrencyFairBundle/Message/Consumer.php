<?php
/**
 * Contains the Consumer
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Message;

use Nc\Bundle\ElephantIOBundle\Service\Client;

/**
 * Class Consumer.
 * Message consumer from queue
 */
class Consumer
{
    /**
     * The queue service
     *
     * @var Queue
     */
    private $queue;

    /**
     * The message processor
     *
     * @var Processor
     */
    private $messageProcessor;

    /**
     * The data extractor
     *
     * @var Extractor
     */
    private $extractor;

    /**
     * Socket.io client
     *
     * @var Client
     */
    private $socketIo;

    /**
     * Get the queue service
     *
     * @return Queue
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * Set the queue service
     *
     * @param Queue $queue
     * @return $this
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;

        return $this;
    }

    /**
     * Get the message processor
     *
     * @return Processor
     */
    public function getMessageProcessor()
    {
        return $this->messageProcessor;
    }

    /**
     * Set the message processor
     *
     * @param Processor $messageProcessor
     * @return $this
     */
    public function setMessageProcessor($messageProcessor)
    {
        $this->messageProcessor = $messageProcessor;

        return $this;
    }

    /**
     * Get data extractor
     *
     * @return Extractor
     */
    public function getExtractor()
    {
        return $this->extractor;
    }

    /**
     * Set data extractor
     *
     * @param Extractor $extractor
     * @return $this
     */
    public function setExtractor($extractor)
    {
        $this->extractor = $extractor;

        return $this;
    }

    /**
     * Get the socket client
     *
     * @return Client
     */
    public function getSocketIo()
    {
        return $this->socketIo;
    }

    /**
     * Set the socket client
     *
     * @param Client $socketIo
     * @return $this
     */
    public function setSocketIo($socketIo)
    {
        $this->socketIo = $socketIo;

        return $this;
    }

    /**
     * Consume messages from queue
     */
    public function consume()
    {
        $queue = $this->getQueue();
        do {
            $message = $queue->getFirstMessage();
            if ($message !== null) {
                if (!$this->getMessageProcessor()->process($message)) {
                    // Requeue message on failure
                    $this->getQueue()->getQueueProvider()->queue($message);
                } else {
                    // Send updates to view
                    $snapshot = $this->getExtractor()->getSnapshot();
                    $this->getSocketIo()->send('update', array(json_encode($snapshot)));
                }
            }
        } while (true);
    }
}
