<?php
/**
 * Contains the Queue
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Message;

use Nc\Bundle\ElephantIOBundle\Service\Client;
use Symfony\Component\Serializer\Serializer;

/**
 * Class Queue.
 * Messages queue processor
 */
class Queue
{
    /** @var QueueProvider */
    private $queueProvider;

    /** @var Serializer */
    private $serializer;

    /**
     * Socket.io client
     *
     * @var Client
     */
    private $socketIo;

    /**
     * Required keys in the message
     *
     * @var array
     */
    private $requiredKeys;

    /**
     * Get the queue provider
     *
     * @return QueueProvider
     */
    public function getQueueProvider()
    {
        return $this->queueProvider;
    }

    /**
     * Set the queue provider
     *
     * @param QueueProvider $queueProvider
     * @return $this
     */
    public function setQueueProvider($queueProvider)
    {
        $this->queueProvider = $queueProvider;

        return $this;
    }

    /**
     * Get the serializer service
     *
     * @return Serializer
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * Set the serializer service
     *
     * @param Serializer $serializer
     * @return $this
     */
    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;

        return $this;
    }

    /**
     * Get the socket client
     *
     * @return Client
     */
    public function getSocketIo()
    {
        return $this->socketIo;
    }

    /**
     * Set the socket client
     *
     * @param Client $socketIo
     * @return $this
     */
    public function setSocketIo($socketIo)
    {
        $this->socketIo = $socketIo;

        return $this;
    }

    /**
     * Get the message required keys
     *
     * @return array
     */
    public function getRequiredKeys()
    {
        return $this->requiredKeys;
    }

    /**
     * Set the message required keys
     *
     * @param array $requiredKeys
     * @return $this
     */
    public function setRequiredKeys($requiredKeys)
    {
        $this->requiredKeys = $requiredKeys;

        return $this;
    }

    /**
     * Queue a message for processing.
     * Validates the message and queue it the queue provider. If the message is queued, push a notification to view
     *
     * @param string $message JSON message
     * @return array
     */
    public function queueMessage($message)
    {
        try {
            $message = $this->validateMessage($message);
            if ($this->getQueueProvider()->queue($message)) {
                $this->getSocketIo()->send('update_queue', array($this->getQueueSize()));
            }
            $result['status'] = true;
        } catch (\Exception $e) {
            $result = array(
                'status' => false,
                'error' => $e->getMessage()
            );
        }

        return $result;
    }

    /**
     * Get and remove the first message to be consumed from queue
     *
     * @return string | null
     */
    public function getFirstMessage()
    {
        return $this->getQueueProvider()->getFirstMessage();
    }

    /**
     * Get the message queue size
     *
     * @return int
     */
    public function getQueueSize()
    {
        return $this->getQueueProvider()->getQueueSize();
    }

    /**
     * Validate a JSON message and the required keys.
     * Unneeded message keys are removed
     *
     * @param string $message JSON message
     * @return string
     * @throws \Exception
     */
    private function validateMessage($message)
    {
        $serializer = $this->getSerializer();
        $messageArray = $serializer->decode($message, 'json');
        $keepKeys = array_flip($this->getRequiredKeys());
        $messageArray = array_intersect_key($messageArray, $keepKeys);
        $requiredKeys = $this->getRequiredKeys();
        $missingKeys = array();
        foreach ($requiredKeys as $key) {
            if (!array_key_exists($key, $messageArray)) {
                $missingKeys[] = $key;
            }
        }
        if (!empty($missingKeys)) {
            throw new \Exception('The following message keys are missing: ' . implode(', ', $missingKeys));
        }
        $this->validateMessageValues($messageArray);

        return $serializer->encode($messageArray, 'json');
    }

    /**
     * Validate each key value agains its type
     *
     * @param array $data Message data array
     * @return bool
     * @throws \Exception
     */
    private function validateMessageValues(array $data)
    {
        foreach ($data as $key => $value) {
            switch ($key) {
                case 'userId':
                    if (false === filter_var($value, FILTER_VALIDATE_INT)
                        || $value === true
                        || $value < 1
                        || $value > 18446744073709551615
                    ) {
                        $validationFails[] = $key;
                    }
                    break;
                case 'currencyFrom':
                    if (!preg_match('/^[A-Z]{3}$/', $value)) {
                        $validationFails[] = $key;
                    }
                    break;
                case 'currencyTo':
                    if (!preg_match('/^[A-Z]{3}$/', $value)) {
                        $validationFails[] = $key;
                    }
                    break;
                case 'amountSell':
                    if (false === filter_var($value, FILTER_VALIDATE_FLOAT)
                        || $value === true
                        || $value <= 0
                        || $value > 18446744073709551615
                    ) {
                        $validationFails[] = $key;
                    }
                    break;
                case 'amountBuy':
                    if (false === filter_var($value, FILTER_VALIDATE_FLOAT)
                        || $value === true
                        || $value <= 0
                        || $value > 18446744073709551615
                    ) {
                        $validationFails[] = $key;
                    }
                    break;
                case 'rate':
                    if (false === filter_var($value, FILTER_VALIDATE_FLOAT)
                        || $value === true
                        || $value <= 0
                        || $value > 18446744073709551615
                    ) {
                        $validationFails[] = $key;
                    }
                    break;
                case 'timePlaced':
                    \DateTime::createFromFormat('d-M-Y H:i:s', $value);
                    $dateTimeErrors = \DateTime::getLastErrors();
                    if (!empty($dateTimeErrors['error_count']) || !empty($dateTimeErrors['warning_count'])) {
                        $validationFails[] = $key;
                    }
                    break;
                case 'originatingCountry':
                    if (!preg_match('/^[A-Z]{2}$/', $value)) {
                        $validationFails[] = $key;
                    }
                    break;
                default:
                    throw new \Exception(sprintf('Unknown message key (%s)', $key));
            }
        }
        if (!empty($validationFails)) {
            throw new \Exception('The following message keys are invalid: ' . implode(', ', $validationFails));
        }

        return true;
    }
}
