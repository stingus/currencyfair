<?php
/**
 * Contains the DefaultController
 *
 * @author Arthur Kerpician <arthur@bluechip.ro>
 */

namespace AK\CurrencyFairBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController.
 * Default controller for CurrencyFairBundle
 *
 * @package AK\CurrencyFairBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Input endpoint API
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function inputAction(Request $request)
    {
        $queueService = $this->get('fair.queue');
        $result = $queueService->queueMessage($request->getContent());

        return new JsonResponse($result);
    }

    /**
     * Inital request for output data.
     * Following updates are sent via socket.io
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function initOutputAction(Request $request)
    {
        $extractorService = $this->get('fair.extractor');
        $result = $extractorService->getSnapshot();

        $response = new JsonResponse($result);
        $origin = sprintf(
            '%s:%s',
            str_replace(':' . $request->getPort(), '', $request->getSchemeAndHttpHost()),
            $this->container->getParameter('nodejs_port')
        );
        $response->headers->set('Access-Control-Allow-Origin', $origin);

        return $response;
    }
}
