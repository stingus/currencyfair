Currency Fair transactions
==========================

# Tech used
CentOS 7  
Symfony 2.6.7  
Redis 3  
Supervisor  
node.js / Socket.io

Project bundle: src/AK/CurrencyFairBundle/

## Unit testing
In the project root, run `phpunit -c app/phpunit.xml.dist`. Project is not fully tested, these are just a demo of functional and unit testing

## Warning
The Wisembly/elephant.io from nc/elephantio-bundle (socket.io client) has a known bug, not resolved until 2015-05-26.  
In production, it's resolved by manually adding the missing line (`$this->session = null;`).  
Details here: https://github.com/Wisembly/elephant.io/issues/97
